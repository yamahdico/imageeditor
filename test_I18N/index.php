﻿<?php

// FETCH IMAGE & WRITE TEXT
$img = imagecreatefromjpeg('text_over_image.jpg');
$white = imagecolorallocate($img, 255, 255, 255);
$txt = "سلام";
$font = "B Nazanin.ttf"; 

require('I18N/Arabic.php');
$Arabic = new I18N_Arabic('Glyphs');

  $text = 'بسم الله الرحمن الرحيم
  بشسیبشسیب
  شسیب سبشسب
  شسیب';
  $text = $Arabic->utf8Glyphs($text);

  imagettftext($img, 20, 0, 10, 100, $blue, $font, $text); 
		  
 // OUTPUT IMAGE
//header('Content-type: image/jpeg');
//imagejpeg($img);
//imagedestroy($jpg_image);

// OR SAVE TO A FILE
// THE LAST PARAMETER IS THE QUALITY FROM 0 to 100
imagejpeg($img, "bg.jpg", 100);

$iw = 400; // image width.
$ih = 400; // image height.

$size = 40;
$angle = 0;
$font = 'arial';
$text = 'العَرَبِيَّة';
  $text = $Arabic->utf8Glyphs($text);


$box = imagettfbbox($size, $angle, $font, $text);
$tw = abs($box[6] - $box[4]); // text width.
$th = abs($box[7] - $box[1]); // text height.

$im = imagecreatetruecolor($iw, $ih);
imagefill($im, 0, 0, 0x00c0c0c0); // set a grey background.

// left aligned.
imagettftext($im, $size, $angle, 0, $th, 0x00000000, $font, $text);

// centre aligned.
imagettftext($im, $size, $angle, ($iw / 2) - ($tw / 2), $th * 2, 0x00000000, $font, $text);

// right aligned.
imagettftext($im, $size, $angle, $iw - $tw, $th * 3, 0x00000000, $font, $text);
imagettftext($im, $size, $angle, $iw - $tw - 5 , $th, 0x00000000, $font, $text);

//header('Content-Type: image/png');
//imagepng($im);
//imagedestroy($im);
imagejpeg($im, "img/test.jpg", 100);
?>
<img src="img/test.jpg">
