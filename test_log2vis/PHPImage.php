<?php
require_once('../src/PHPImage.php');
$bg = 'lo15.jpg';
$image = new PHPImage();
$image->setDimensionsFromImage($bg);
$image->draw($bg);

$image->setFont('font/calibri.ttf');
$image->setStrokeWidth(1);
$width = ($image->getWidth()*24)/100;
$height = ($image->getHeight()*9)/100;
$x = ($image->getWidth()*73)/100;
$y = ($image->getHeight()*88)/100;
$image->rectangle($x, $y  , $width , $height, array(0, 0, 0), 0.4);
$image->textBox('www.fadak.ir @cm.fadak.ir', array(
	'width' => $width,
	'height' => $height,
	'x' => $x+2,
	'y' => $y+4,
	//'debug' => true,
	'fontSize' => 30
));

$image->setFont('font/BZiba.ttf');
$image->setAlignHorizontal('center'); // right
$image->setAlignVertical('center'); //top
$image->setTextColor(array(0, 0, 0));
$image->setStrokeWidth(0);
$width = ($image->getWidth()*70)/100;
$height = ($image->getHeight()*70)/100;
$x = ($image->getWidth()-$width)/2;
$y = ($image->getHeight()-$height)/2;
$image->rectangle($x, $y  , $width , $height, array(255, 255, 255), 0.5);
$text = "ایــن متن برای تست می‌باشدایــن متن برا
ﺗﻔﻜﺮ ﻫﺎﻳﺪﮔﺮ منظر و مفسری از ﺳﻨﺖ ﻣﺘﺎﻓﻴﺰﻳک ﻏﺮبی و ﺗﻼشی ﺑﻪ ﻣﻨﻈﻮر ﮔﺬر از آن اﺳﺖ، و در ﻣﻮاﺟﻬﻪ ﺑﺎ اﻳﻦ ﺗﻔﻜﺮ ﮔﻮیی ﺗﻤﺎم ﺗﻔﻜﺮ ﻫﺎﻳﺪﮔﺮ منظر و مفسری از ﺳﻨﺖ ﻣﺘﺎﻓﻴﺰﻳک ﻏﺮبی و ﺗﻼشی ﺑﻪ ﻣﻨﻈﻮر ﮔﺬر از آن اﺳﺖ، و در ﻣﻮاﺟﻬﻪ ﺑﺎ اﻳﻦ ﺗﻔﻜﺮ ﮔﻮیی ﺗﻤﺎم";
// Convert for unicode
require_once('persian_log2vis/persian_log2vis.php');
persian_log2vis($text);
$image->textBox($text, array(
	'width' => $width,
	'height' => $height,
	'x' => $x,
	'y' => $y,
	'alignHorizontal' => 'right',
	'alignVertical' => 'top',
	//'debug' => true,
	'fontSize' => 30
));	
$image->show();